-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2021 at 09:16 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk_inventariskominfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `alternatif`
--

CREATE TABLE `alternatif` (
  `id` varchar(32) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `keterangan` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_by` varchar(64) NOT NULL,
  `modified_by` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alternatif`
--

INSERT INTO `alternatif` (`id`, `nama`, `keterangan`, `created_at`, `modified_at`, `created_by`, `modified_by`) VALUES
('35c288acf3d14c47abfa9daf92e3e483', 'A2', 'Fuller', '2021-07-05 09:10:45', '2021-07-05 09:10:45', 'admin', 'admin'),
('a24f333be95141b9896f08d670027324', 'A1', 'Davolio', '2021-07-05 09:10:35', '2021-07-05 09:10:35', 'admin', 'admin'),
('c8dd253e7a9948339341fc72721cc731', 'A5', 'Anis', '2021-07-05 09:13:48', '2021-07-05 09:13:48', 'admin', 'admin'),
('d997b7dae67a4c5c82a4866bf88c1cc0', 'A3', 'Leverling', '2021-07-05 09:10:57', '2021-07-05 09:10:57', 'admin', 'admin'),
('df7b83f003e549cc9a75ee4971924854', 'A4', 'Peacock', '2021-07-05 09:11:07', '2021-07-05 09:11:07', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `timestamp` int(10) UNSIGNED DEFAULT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('brmjocnuo6sp6kaq1p76d5cn3d0r2hpi', '::1', 1625468598, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353436383539383b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('q95g8pv6ig4msol309b0789c2r1ldj4a', '::1', 1625468899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353436383839393b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('n0s1fuflero2sgd7uuv18vgt3qnr47pp', '::1', 1625469217, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353436393231373b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b),
('ed9q58onpkfc5ncpa2nk60of00uhesqc', '::1', 1625469289, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353436393231373b73657373696f6e5f6c706f7772745f7573657269647c733a33323a223663613530653466313530653465363139306232386264396266306364313563223b73657373696f6e5f6c706f7772745f756e616d657c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6465736b72697073697c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6e616d617c733a353a2261646d696e223b73657373696f6e5f6c706f7772745f6c6f676765645f696e7c623a313b);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` varchar(32) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(96) NOT NULL,
  `atribut` tinyint(4) NOT NULL COMMENT '1=benefit, 2=cost',
  `bobot` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_by` varchar(64) NOT NULL,
  `modified_by` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `kode`, `nama`, `atribut`, `bobot`, `created_at`, `modified_at`, `created_by`, `modified_by`) VALUES
('0485ef15b43e44ffaa7f43f1926ed2db', 'C1', 'Penghasilan Orang Tua', 2, 25, '2021-07-05 08:59:51', '2021-07-05 08:59:51', 'admin', 'admin'),
('56884c403b9d4b818fe611d81b226387', 'C2', 'Semester', 1, 20, '2021-07-05 09:00:06', '2021-07-05 09:00:06', 'admin', 'admin'),
('6e778229b7f94e3fb6221d165c1537c7', 'C5', 'Nilai', 1, 30, '2021-07-05 09:01:10', '2021-07-05 09:01:10', 'admin', 'admin'),
('72b53c5494544a0fa5398ee7bfba4289', 'C4', 'Saudara Kandung', 1, 10, '2021-07-05 09:00:46', '2021-07-05 09:00:46', 'admin', 'admin'),
('96cc54f408e44cd696efafd91e73fdb6', 'C3', 'Tanggungan Orang Tua', 1, 15, '2021-07-05 09:00:27', '2021-07-05 09:00:27', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `loguser`
--

CREATE TABLE `loguser` (
  `logid` int(11) NOT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  `logact` varchar(11) DEFAULT NULL,
  `logip` varchar(15) NOT NULL,
  `logplatagent` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loguser`
--

INSERT INTO `loguser` (`logid`, `userid`, `logdate`, `logact`, `logip`, `logplatagent`) VALUES
(1, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-04 10:47:54', 'login', '::1', '(Windows 10) Opera 77.0.4054.172'),
(2, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-04 11:10:37', 'login', '::1', '(Windows 10) Opera 77.0.4054.172'),
(3, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-04 17:12:27', 'login', '::1', '(Windows 10) Opera 77.0.4054.172'),
(4, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-05 09:21:31', 'login', '::1', '(Windows 10) Opera 77.0.4054.172'),
(5, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-05 13:54:36', 'login', '::1', '(Windows 10) Opera 77.0.4054.172'),
(6, '6ca50e4f150e4e6190b28bd9bf0cd15c', '2021-07-05 13:58:16', 'login', '::1', '(Windows 10) Opera 77.0.4054.172');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_alternatif`
--

CREATE TABLE `nilai_alternatif` (
  `id_subkriteria` varchar(32) NOT NULL,
  `id_alternatif` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_alternatif`
--

INSERT INTO `nilai_alternatif` (`id_subkriteria`, `id_alternatif`) VALUES
('b281e2c08b8d4f4db9abfe8971c0b38e', 'a24f333be95141b9896f08d670027324'),
('5a96cffa61ae40d3b44e4bdc2fd04bca', 'a24f333be95141b9896f08d670027324'),
('1cf893df57f54d089b06f5a4cdbaba87', 'a24f333be95141b9896f08d670027324'),
('5f1b69a79ff34b36b8c63542d809f084', 'a24f333be95141b9896f08d670027324'),
('f9128ab7d0704adc8e3b74fb1d81ee35', 'a24f333be95141b9896f08d670027324'),
('8f45810a612f4304b17bd109b6bbceb5', '35c288acf3d14c47abfa9daf92e3e483'),
('3ebf2aaa8a67470bae10a295269feb98', '35c288acf3d14c47abfa9daf92e3e483'),
('22d1f164db314634a3cacf4f2579575a', '35c288acf3d14c47abfa9daf92e3e483'),
('23e7f5a2655d415c992ecad0be04708f', '35c288acf3d14c47abfa9daf92e3e483'),
('1ca987727ddf402f881543aa69e7d543', '35c288acf3d14c47abfa9daf92e3e483'),
('7c9c72dd4c4d4976bd9445ab4cfe09ca', 'd997b7dae67a4c5c82a4866bf88c1cc0'),
('caba343a13f14821a24c7d70db810bc1', 'd997b7dae67a4c5c82a4866bf88c1cc0'),
('5b633cac690c4e8aac2413daca9edba4', 'd997b7dae67a4c5c82a4866bf88c1cc0'),
('cd1b6452713241719fff8007443d1a44', 'd997b7dae67a4c5c82a4866bf88c1cc0'),
('911416039463446c80641f7d3512bc9a', 'd997b7dae67a4c5c82a4866bf88c1cc0'),
('6359b3c57ff0462fb159f1c2ab87afdc', 'df7b83f003e549cc9a75ee4971924854'),
('5a96cffa61ae40d3b44e4bdc2fd04bca', 'df7b83f003e549cc9a75ee4971924854'),
('c3ad0263cebf4791b3e335747afd92db', 'df7b83f003e549cc9a75ee4971924854'),
('a2b88cd1aaa7493cb20d0bf24f3a1606', 'df7b83f003e549cc9a75ee4971924854'),
('1105c54c043c4a3d9d50572f06d2928e', 'df7b83f003e549cc9a75ee4971924854'),
('7c9c72dd4c4d4976bd9445ab4cfe09ca', 'c8dd253e7a9948339341fc72721cc731'),
('3ebf2aaa8a67470bae10a295269feb98', 'c8dd253e7a9948339341fc72721cc731'),
('5b633cac690c4e8aac2413daca9edba4', 'c8dd253e7a9948339341fc72721cc731'),
('5f1b69a79ff34b36b8c63542d809f084', 'c8dd253e7a9948339341fc72721cc731'),
('1ca987727ddf402f881543aa69e7d543', 'c8dd253e7a9948339341fc72721cc731');

-- --------------------------------------------------------

--
-- Table structure for table `subkriteria`
--

CREATE TABLE `subkriteria` (
  `id` varchar(32) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `isi` varchar(96) NOT NULL,
  `nilai` int(11) NOT NULL,
  `id_kriteria` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_by` varchar(64) NOT NULL,
  `modified_by` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkriteria`
--

INSERT INTO `subkriteria` (`id`, `nama`, `isi`, `nilai`, `id_kriteria`, `created_at`, `modified_at`, `created_by`, `modified_by`) VALUES
('080cb8d3bd374bfe89d59af18aaa1232', '> Rp 4.500.000', '> Rp 4.500.000', 100, '0485ef15b43e44ffaa7f43f1926ed2db', '2021-07-05 09:03:49', '2021-07-05 09:03:49', 'admin', 'admin'),
('0be2680ecbf248ee994ff393b520b297', '<= Rp 1.000.000', '<= Rp 1.000.000', 20, '0485ef15b43e44ffaa7f43f1926ed2db', '2021-07-05 09:02:50', '2021-07-05 09:02:50', 'admin', 'admin'),
('1105c54c043c4a3d9d50572f06d2928e', '4 Orang', '4 Orang', 80, '96cc54f408e44cd696efafd91e73fdb6', '2021-07-05 09:07:14', '2021-07-05 09:07:14', 'admin', 'admin'),
('1ca987727ddf402f881543aa69e7d543', '2 Orang', '2 Orang', 40, '96cc54f408e44cd696efafd91e73fdb6', '2021-07-05 09:05:44', '2021-07-05 09:05:44', 'admin', 'admin'),
('1cf893df57f54d089b06f5a4cdbaba87', '1 Orang', '1 Orang', 20, '72b53c5494544a0fa5398ee7bfba4289', '2021-07-05 09:07:50', '2021-07-05 09:07:50', 'admin', 'admin'),
('22d1f164db314634a3cacf4f2579575a', '2 Orang', '2 Orang', 40, '72b53c5494544a0fa5398ee7bfba4289', '2021-07-05 09:08:00', '2021-07-05 09:08:00', 'admin', 'admin'),
('23e7f5a2655d415c992ecad0be04708f', 'Semester 5', 'Semester 5', 40, '56884c403b9d4b818fe611d81b226387', '2021-07-05 09:04:26', '2021-07-05 09:04:26', 'admin', 'admin'),
('3ebf2aaa8a67470bae10a295269feb98', '<= Rp 1.500.000', '<= Rp 1.500.000', 40, '0485ef15b43e44ffaa7f43f1926ed2db', '2021-07-05 09:03:03', '2021-07-05 09:03:03', 'admin', 'admin'),
('4c2ef289be67429989b2f188165c1ebe', '> 4 Orang', '> 4 Orang', 100, '72b53c5494544a0fa5398ee7bfba4289', '2021-07-05 09:08:39', '2021-07-05 09:08:39', 'admin', 'admin'),
('5a96cffa61ae40d3b44e4bdc2fd04bca', '<= Rp 4.500.000', '<= Rp 4.500.000', 80, '0485ef15b43e44ffaa7f43f1926ed2db', '2021-07-05 09:03:34', '2021-07-05 09:03:34', 'admin', 'admin'),
('5b633cac690c4e8aac2413daca9edba4', '3 Orang', '3 Orang', 60, '72b53c5494544a0fa5398ee7bfba4289', '2021-07-05 09:08:16', '2021-07-05 09:08:16', 'admin', 'admin'),
('5f1b69a79ff34b36b8c63542d809f084', 'Semester 4', 'Semester 4', 20, '56884c403b9d4b818fe611d81b226387', '2021-07-05 09:04:15', '2021-07-05 09:04:15', 'admin', 'admin'),
('6359b3c57ff0462fb159f1c2ab87afdc', '< 3,5', '< 3,5', 80, '6e778229b7f94e3fb6221d165c1537c7', '2021-07-05 09:09:51', '2021-07-05 09:09:51', 'admin', 'admin'),
('7c9c72dd4c4d4976bd9445ab4cfe09ca', '>= 3,5', '>= 3,5', 100, '6e778229b7f94e3fb6221d165c1537c7', '2021-07-05 09:10:02', '2021-07-05 09:10:02', 'admin', 'admin'),
('8f45810a612f4304b17bd109b6bbceb5', '< 3', '< 3', 40, '6e778229b7f94e3fb6221d165c1537c7', '2021-07-05 09:09:29', '2021-07-05 09:09:29', 'admin', 'admin'),
('911416039463446c80641f7d3512bc9a', '3 Orang', '3 Orang', 60, '96cc54f408e44cd696efafd91e73fdb6', '2021-07-05 09:07:01', '2021-07-05 09:07:01', 'admin', 'admin'),
('a2b88cd1aaa7493cb20d0bf24f3a1606', 'Semester 7', 'Semester 7', 80, '56884c403b9d4b818fe611d81b226387', '2021-07-05 09:04:52', '2021-07-05 09:04:52', 'admin', 'admin'),
('a7d606e94b0a43789d12e90cfe1b0e85', 'Semester 8', 'Semester 8', 100, '56884c403b9d4b818fe611d81b226387', '2021-07-05 09:05:06', '2021-07-05 09:05:06', 'admin', 'admin'),
('b281e2c08b8d4f4db9abfe8971c0b38e', '< 2,75', '< 2,75', 20, '6e778229b7f94e3fb6221d165c1537c7', '2021-07-05 09:09:02', '2021-07-05 09:09:02', 'admin', 'admin'),
('c2523bb6309e4cccabc69bd85409ea41', '< 3,25', '< 3,25', 60, '6e778229b7f94e3fb6221d165c1537c7', '2021-07-05 09:09:39', '2021-07-05 09:09:39', 'admin', 'admin'),
('c3ad0263cebf4791b3e335747afd92db', '4 Orang', '4 Orang', 80, '72b53c5494544a0fa5398ee7bfba4289', '2021-07-05 09:08:28', '2021-07-05 09:08:28', 'admin', 'admin'),
('caba343a13f14821a24c7d70db810bc1', '<= Rp 3.000.000', '<= Rp 3.000.000', 60, '0485ef15b43e44ffaa7f43f1926ed2db', '2021-07-05 09:03:18', '2021-07-05 09:03:18', 'admin', 'admin'),
('cd1b6452713241719fff8007443d1a44', 'Semester 6', 'Semester 6', 60, '56884c403b9d4b818fe611d81b226387', '2021-07-05 09:04:41', '2021-07-05 09:04:41', 'admin', 'admin'),
('db6036f6de324a3eac9f953b3cf831e9', '> 4 Orang', '> 4 Orang', 100, '96cc54f408e44cd696efafd91e73fdb6', '2021-07-05 09:07:28', '2021-07-05 09:07:28', 'admin', 'admin'),
('f9128ab7d0704adc8e3b74fb1d81ee35', '1 Orang', '1 Orang', 20, '96cc54f408e44cd696efafd91e73fdb6', '2021-07-05 09:05:34', '2021-07-05 09:05:34', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` varchar(32) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `deskripsi` varchar(99) NOT NULL,
  `ustate` tinyint(1) NOT NULL,
  `ucreated` datetime NOT NULL,
  `umodified` datetime NOT NULL,
  `udeleted` datetime NOT NULL,
  `ucreatedby` varchar(100) NOT NULL,
  `umodifiedby` varchar(100) NOT NULL,
  `udeletedby` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `nama`, `email`, `pass`, `uname`, `deskripsi`, `ustate`, `ucreated`, `umodified`, `udeleted`, `ucreatedby`, `umodifiedby`, `udeletedby`) VALUES
('6ca50e4f150e4e6190b28bd9bf0cd15c', 'admin', '', '$2y$10$SNGgprCem2RaZT5h3uv2YezBsP5lLNWxH.90l4enV3YMEMXNiRFvq', 'admin', 'admin', 1, '2020-12-13 14:19:16', '2020-12-13 14:19:16', '0000-00-00 00:00:00', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alternatif`
--
ALTER TABLE `alternatif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loguser`
--
ALTER TABLE `loguser`
  ADD PRIMARY KEY (`logid`),
  ADD KEY `fk_loguser` (`userid`);

--
-- Indexes for table `nilai_alternatif`
--
ALTER TABLE `nilai_alternatif`
  ADD KEY `fk_subkriteria` (`id_subkriteria`),
  ADD KEY `fk_alternatif` (`id_alternatif`);

--
-- Indexes for table `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kriteria` (`id_kriteria`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loguser`
--
ALTER TABLE `loguser`
  MODIFY `logid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nilai_alternatif`
--
ALTER TABLE `nilai_alternatif`
  ADD CONSTRAINT `fk_alternatif` FOREIGN KEY (`id_alternatif`) REFERENCES `alternatif` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subkriteria` FOREIGN KEY (`id_subkriteria`) REFERENCES `subkriteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD CONSTRAINT `fk_kriteria` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
