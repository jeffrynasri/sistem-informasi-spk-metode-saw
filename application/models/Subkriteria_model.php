<?php

class Subkriteria_model extends CI_Model
{
    // id
    // nama
    // isi
    // nilai
    // id_kriteria

    var $table = 'subkriteria';

    var $column_order = array('subkriteria.nama','subkriteria.isi','subkriteria.nilai','subkriteria.id_kriteria'); //set column field database for datatable orderable
    var $column_search = array('subkriteria.nama','subkriteria.isi','subkriteria.nilai','subkriteria.id_kriteria');//set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('subkriteria.nama' => 'asc'); // default order
  
    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array())
    {
      $this->db->where($params);
      $this->db->select('subkriteria.*');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
  
    function get_datatables($params=array())
    {
      $this->_get_datatables_query( $params?$params:array() );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array())
    {
      $this->_get_datatables_query($params);
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array())
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get subkriteria by id
     */
    function get_subkriteria($id)
    {
        $this->db->select('subkriteria.*,
        kriteria.kode as kriteria_kode,
        kriteria.nama as kriteria_nama,
        kriteria.atribut as kriteria_atribut,
        kriteria.bobot as kriteria_bobot');
        $this->db->join('kriteria', 'subkriteria.id_kriteria = kriteria.id');
  
        return $this->db->get_where('subkriteria',array('subkriteria.id'=>$id))->row_array();
    }
        
    /*
     * Get all subkriteria
     */
    function get_all_subkriteria($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->select('subkriteria.*,
        kriteria.kode as kriteria_kode,
        kriteria.nama as kriteria_nama,
        kriteria.atribut as kriteria_atribut,
        kriteria.bobot as kriteria_bobot');
        $this->db->join('kriteria', 'subkriteria.id_kriteria = kriteria.id');
        $this->db->where($params);
        return $this->db->get('subkriteria')->result_array();
    }
        
    /*
     * function to add new subkriteria
     */
    function add_subkriteria($params)
    {
        $this->db->insert('subkriteria',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update subkriteria
     */
    function update_subkriteria($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('subkriteria',$params);
    }
    
    /*
     * function to delete subkriteria
     */
    function delete_subkriteria($id)
    {
        return $this->db->delete('subkriteria',array('id'=>$id));
    }
}
