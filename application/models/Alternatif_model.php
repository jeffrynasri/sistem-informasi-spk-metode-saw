<?php

class Alternatif_model extends CI_Model
{
    // nama
    // keterangan
    var $table = 'alternatif';

    var $column_order = array('alternatif.nama','alternatif.keterangan'); //set column field database for datatable orderable
    var $column_search = array('alternatif.nama','alternatif.keterangan');//set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('alternatif.nama' => 'asc'); // default order
  
    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array())
    {
      $this->db->where($params);
      $this->db->select('alternatif.*');
  
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
  
    function get_datatables($params=array())
    {
      $this->_get_datatables_query( $params?$params:array() );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array())
    {
      $this->_get_datatables_query($params);
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array())
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get alternatif by id
     */
    function get_alternatif($id)
    {
        return $this->db->get_where('alternatif',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all alternatif
     */
    function get_all_alternatif($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->where($params);
        return $this->db->get('alternatif')->result_array();
    }
        
    /*
     * function to add new alternatif
     */
    function add_alternatif($params)
    {
        $this->db->insert('alternatif',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update alternatif
     */
    function update_alternatif($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('alternatif',$params);
    }
    
    /*
     * function to delete alternatif
     */
    function delete_alternatif($id)
    {
        return $this->db->delete('alternatif',array('id'=>$id));
    }
}
