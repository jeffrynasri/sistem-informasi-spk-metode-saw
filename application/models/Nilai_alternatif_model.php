<?php

class Nilai_alternatif_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get nilai_alternatif by 
     */
    function get_nilai_alternatif($params)
    {
        $this->db->select('alternatif.nama as alternatif_nama,
        subkriteria.nama as subkriteria_nama,
        subkriteria.isi as subkriteria_isi,
        subkriteria.nilai as subkriteria_nilai,
        subkriteria.id_kriteria as subkriteria_id_kriteria,
        kriteria.nama as kriteria_nama,
        kriteria.atribut as kriteria_atribut,
        kriteria.bobot as kriteria_bobot');
        $this->db->join('alternatif', 'nilai_alternatif.id_alternatif = alternatif.id');
        $this->db->join('subkriteria', 'nilai_alternatif.id_subkriteria = subkriteria.id');
        $this->db->join('kriteria', 'subkriteria.id_kriteria = kriteria.id');
        return $this->db->get_where('nilai_alternatif',$params)->row_array();
    }
        
    /*
     * Get all nilai_alternatif
     */
    function get_all_nilai_alternatif($params=array())
    {
        $this->db->select('alternatif.nama as alternatif_nama,
        subkriteria.nama as subkriteria_nama,
        subkriteria.isi as subkriteria_isi,
        subkriteria.nilai as subkriteria_nilai,
        subkriteria.id_kriteria as subkriteria_id_kriteria,
        kriteria.nama as kriteria_nama,
        kriteria.atribut as kriteria_atribut,
        kriteria.bobot as kriteria_bobot');
        $this->db->join('alternatif', 'nilai_alternatif.id_alternatif = alternatif.id');
        $this->db->join('subkriteria', 'nilai_alternatif.id_subkriteria = subkriteria.id');
        $this->db->join('kriteria', 'subkriteria.id_kriteria = kriteria.id');
        $this->db->where($params);

        return $this->db->get('nilai_alternatif')->result_array();
    }
        
    /*
     * function to add new nilai_alternatif
     */
    function add_nilai_alternatif($params)
    {
        return $this->db->insert('nilai_alternatif',$params);
    }
    
    /*
     * function to update nilai_alternatif
     */
    function update_nilai_alternatif($params,$where)
    {
        $this->db->where($where);
        return $this->db->update('nilai_alternatif',$params);
    }
    
    /*
     * function to delete nilai_alternatif
     */
    function delete_nilai_alternatif($where)
    {
        return $this->db->delete('nilai_alternatif',$where);
    }
}
