<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Dashboard
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info btn-info">
                        <div class="inner">
                            <h3><?php echo count($kriteria); ?></h3>
                            <p>Kriteria</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list"></i>
                        </div>
                        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success btn-success">
                        <div class="inner">
                            <h3><h3><?php echo count($subkriteria); ?></h3></h3>
                            <p>Subkriteria</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list"></i>
                        </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning btn-warning">
                        <div class="inner">
                            <h3><h3><?php echo count($alternatif); ?></h3></h3>
                            <p>Alternatif</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger btn-danger">
                        <div class="inner">
                            <h3><h3><?php echo count($user); ?></h3></h3>
                            <p>User</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    </div>
            </div>
        </div>
    </div>
</div>