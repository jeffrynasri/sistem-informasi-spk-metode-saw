<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Alternatif</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('alternatif/add'); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Keterangan</th>
                      <th>Kriteria</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
                                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('alternatif/get_data_alternatif_json')?>",
      "type": "POST"

    },
    "columnDefs": [
      {
        "width":"20%",
        "targets": [ 0 ],
      },
      {
        "width":"20%",
        "targets": [ 1 ],
      },
      {
        "width":"50%",
        "targets": [ 2 ],
        "orderable": false,
      },
      {
        "width":"10%",
        "targets": [ 3 ],
        "orderable": false,
      },
    ],


  });

});

</script>
