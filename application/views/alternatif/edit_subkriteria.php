<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Isi Penilaian Alternatif</h3>
            </div>
			<?php echo form_open('alternatif/edit_subkriteria/'.$alternatif['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<?php foreach($kriteria as $kr){ ?>
						<div class="col-md-12">
							<label for="id_subkriteria" class="control-label"><span class="text-danger">*</span><?php echo $kr['nama']; ?></label>
							<div class="form-group">
								<select name="id_subkriteria[]"  class="form-control" id="id_subkriteria">
									<?php foreach($kr['subkriteria'] as $skr){ ?>
										<option value='<?php echo $skr['id']; ?>'><?php echo $skr['nama'];?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<input type="hidden" name="pancingan" value="tes" class="form-control" id="pancingan" />
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>