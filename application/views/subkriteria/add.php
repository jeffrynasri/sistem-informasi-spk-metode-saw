<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Tambah Subkriteria <?php echo $kriteria['nama'];?></h3>
            </div>
            <?php echo form_open('subkriteria/add/'.$kriteria['id']); ?>
          	<div class="box-body">
          		<div class="row clearfix">
				  	<ol class="breadcrumb">
						<li><a href="<?php echo site_url('kriteria/index'); ?>"><i class="fa fa-dashboard"></i> Kriteria</a></li>
						<li><a href="<?php echo site_url('subkriteria/index/'.$kriteria['id']); ?>"><i class="fa fa-dashboard"></i> Subkriteria</a></li>
						<li class="active">Tambah Subkriteria <?php echo $kriteria['nama'];?></li>
					</ol>
					<div class="col-md-12">
						<label for="nama" class="control-label"><span class="text-danger">*</span>Nama</label>
						<div class="form-group">
							<input type="text" name="nama" value="<?php echo $this->input->post('nama'); ?>" class="form-control" id="nama" />
							<span class="text-danger"><?php echo form_error('nama');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="isi" class="control-label"><span class="text-danger">*</span>Isi</label>
						<div class="form-group">
							<input type="text" name="isi" value="<?php echo $this->input->post('isi'); ?>" class="form-control" id="isi" />
							<span class="text-danger"><?php echo form_error('isi');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="nilai" class="control-label"><span class="text-danger">*</span>Nilai</label>
						<div class="form-group">
							<input type="number" name="nilai" value="<?php echo $this->input->post('nilai'); ?>" class="form-control" id="nilai" />
							<span class="text-danger"><?php echo form_error('nilai');?></span>
						</div>
					</div>
					<!-- <input type="text" hidden name="id_kriteria" value="<?php echo $id_kriteria; ?>" class="form-control" id="id_kriteria" /> -->
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Simpan
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>