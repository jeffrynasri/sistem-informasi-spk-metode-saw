<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Subkriteria <?php echo $kriteria['nama'];?></h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('subkriteria/add/'.$kriteria['id']); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <ol class="breadcrumb">
                  <li><a href="<?php echo site_url('kriteria/index'); ?>"><i class="fa fa-dashboard"></i> Kriteria</a></li>
                  <li class="active">Subkriteria <?php echo $kriteria['nama'];?></li>
                </ol>
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Isi</th>
                      <th>Nilai</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
                                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('subkriteria/get_data_subkriteria_json/'.$kriteria['id'])?>",
      "type": "POST"

    },
    "columnDefs": [
      {
        "targets": [ 1,3 ],
        "orderable": false,
      },
    ],


  });

});

</script>
