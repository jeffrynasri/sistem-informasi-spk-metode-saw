<div class="row">
    <div class="col-md-12">
        <h3 class="box-title">Nilai Alternatif </h3>
    </div>
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Persiapan </h3>
            	<div class="box-tools">
                    <!-- <a href="<?php echo site_url('nilai_alternatif/add'); ?>" class="btn btn-success btn-sm">Add</a>  -->
                </div>
            </div>
            <?php echo form_open('penilaian/index');?>
                <div class="box-body">
                    <h3>Matriks Asli </h3>
                    <table class="table table-stripped">
                        <tr>
                            <?php foreach($nakol_kriteria as $nakri){ ?>
                                <td><?php echo $nakri; ?></td>
                            <?php } ?>
                        <tr>
                        <?php foreach($matriks_asli as $matas){ ?>
                            <tr>
                                <?php foreach($matas as $mts){ ?>
                                    <td><?php echo $mts; ?></td>
                                <?php } ?>
                            <tr>
                        <?php } ?>
                    </table>
                    <h3>Matriks Konversi</h3>
                    <table class="table table-stripped">
                        <tr>
                            <?php foreach($nakol_kriteria as $nakri){ ?>
                                <td><?php echo $nakri; ?></td>
                            <?php } ?>
                        <tr>
                        <?php foreach($matriks_konversi as $makon){ ?>
                            <tr>
                                <?php foreach($makon as $mkn){ ?>
                                    <td><?php echo $mkn; ?></td>
                                <?php } ?>
                            <tr>
                        <?php } ?>
                    </table>
                </div>
                <div class="box-footer">
                    <input type="hidden" name="hitung" id="hitung" value="hitung"/>
                    <button type="submit" class="button btn btn-primary">Hitung</button>
                </div>
            <?php echo form_close();?>  
        </div>
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Pengerjaan</h3>

              <div class="box-tools pull-right">
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12">
                    <?php if(isset($hasil_normalisasi)){ ?>
                        <h3>Normalisasi</h3>
                        <table class="table table-stripped">
                            <tr>
                                <?php foreach($nakol_kriteria as $nakri){ ?>
                                    <td><?php echo $nakri; ?></td>
                                <?php } ?>
                            <tr>
                            <?php $i=0; ?>
                            <?php foreach($hasil_normalisasi as $hanor){ ?>
                                <tr>
                                    <td><?php echo $nabar_alternatif[$i]; ?></td>
                                    <?php foreach($hanor as $key_hanor=>$value_hanor){ ?>
                                        <td><?php echo $value_hanor; ?></td>
                                    <?php } ?>
                                <tr>
                                <?php $i++; ?>
                            <?php } ?>
                        </table>
                    <?php } ?>
                    <?php if(isset($hasil_perangkingan)){ ?>
                        <h3>Perangkingan</h3>
                        <table class="table table-stripped">
                            <?php $i=0; ?>
                            <?php foreach($hasil_perangkingan as $hape){ ?>
                                <tr>
                                <td><?php echo $nabar_alternatif[$i]; ?></td>
                                    <td><?php echo $hape; ?></td>
                                <tr>
                                <?php $i++; ?>
                            <?php } ?>
                        </table>
                    <?php } ?>
                    <?php if(isset($hasil_kesimpulan)){ ?>
                        <h3>Kesimpulan</h3>
                        <table class="table table-stripped">
                            <?php $i=0; ?>
                            <?php foreach($hasil_kesimpulan as $hakes){ ?>
                                <tr>
                                    <td><?php echo $hakes['nama']; ?></td>
                                    <td><?php echo $hakes['hasil']; ?></td>
                                <tr>
                                <?php $i++; ?>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>
                
                
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
