<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ubah Kriteria</h3>
            </div>
			<?php echo form_open('kriteria/edit/'.$kriteria['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-12">
						<label for="kode" class="control-label"><span class="text-danger">*</span>Kode</label>
						<div class="form-group">
							<input type="text" name="kode" value="<?php echo ($this->input->post('kode') ? $this->input->post('kode') : $kriteria['kode']); ?>" class="form-control" id="kode" />
							<span class="text-danger"><?php echo form_error('kode');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="nama" class="control-label"><span class="text-danger">*</span>Nama</label>
						<div class="form-group">
							<input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $kriteria['nama']); ?>" class="form-control" id="nama" />
							<span class="text-danger"><?php echo form_error('nama');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="atribut" class="control-label"><span class="text-danger">*</span>Atribut</label>
						<div class="form-group">
							<select name="atribut" class="form-control">
								<?php 
								$atribut_values = array(
									'1'=>'Benefit',
									'2'=>'Cost',
								);

								foreach($atribut_values as $value => $display_text)
								{
									$selected = ($value == $kriteria['atribut']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('atribut');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="bobot" class="control-label"><span class="text-danger">*</span>Bobot</label>
						<div class="form-group">
							<input type="number" name="bobot" value="<?php echo ($this->input->post('bobot') ? $this->input->post('bobot') : $kriteria['bobot']); ?>" class="form-control" id="bobot" />
							<span class="text-danger"><?php echo form_error('bobot');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>