<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Kriteria</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('kriteria/add'); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Atribut</th>
                        <th>Bobot</th>
                        <th>Subkriteria</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('kriteria/get_data_kriteria_json')?>",
      "type": "POST"

    },
    "columnDefs": [
      {
        "targets": [ 4,5 ],
        "orderable": false,
      },
    ],


  });

});

</script>
