<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?php echo APP_NAME; ?></title>
	<!-- ICON -->
	<link rel='icon' href="<?php echo site_url('resources/images/flag_indo_profile.png');?>"/>
  <style>
     body {
         display: flex;
         min-height: 100vh;
         flex-direction: column;
     }
     main {
         flex: 1 0 auto;
     }
  </style>

	<!-- Materialize -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>resources/css/materialize.min.css"  media="screen,projection"/>
	<script src="<?php echo base_url();?>resources/js/materialize.min.js"></script>

	<!-- Jquery 3.3.1 -->
	<script src="<?php echo base_url();?>resources/js/jquery-3.3.1.js"></script>

	<!-- DataTable 1.10.19-->
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/material.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/responsive.dataTables.css">
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/buttons.dataTables.min.css">
	<script src="<?php echo base_url();?>resources/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/dataTables.responsive.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>resources/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/jszip.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/vfs_fonts.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url();?>resources/js/buttons.print.min.js"></script>

	<!-- Serailize 0.12.6-->
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/selectize.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="<?php echo base_url();?>resources/js/selectize.js"></script>
</head>
<body>
  <?php $this->load->view('layouts/home_header'); ?>
  <main>
    <?php
    if(isset($_view) && $_view) $this->load->view($_view);
    ?>
  </main>
  <?php $this->load->view('layouts/home_footer'); ?>
</body>
<script type="text/javascript">
  $(document).ready(function() {
    $('.select-state').selectize({
      maxItems: 3
    });
  });
</script>

<script>
  $(function () {
    $('#datatable').DataTable({
			dom: 'Bfrtip',
				buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print'
				],
			"bLengthChange": false,
			"pageLength": 20,
      "language": {
        "lengthMenu": "Tampilkan _MENU_ Data per Halaman",
        "zeroRecords": "Data Tidak Ditemukan.",
        "info": "Halaman Ke- _PAGE_ Dari _PAGES_",
        "infoEmpty": "Halaman Ke- 0 Dari 0",
        "infoFiltered": "(terfilter dari _MAX_ data)",
        "loadingRecords": "Mohon Tunggu...",
        "processing": "Sedang Diproses...",
        "search": "Cari:",
        "bSort": true,
        "pageLength":5,
        "paginate": {
          "previous": "Sebelumnya",
          "next": "Selanjutnya"
        }

      }
    })
  })
</script>
</html>
<!-- {
    initComplete: function () {
      $('select[name="example_length"]').addClass('mdb-select');
      //$('.mdb-select').material_select();
    }
  } -->
