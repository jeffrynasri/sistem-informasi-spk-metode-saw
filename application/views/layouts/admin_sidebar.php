<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <!-- <li class="header">Kriteria</li> -->
        <li class="<?php if($this->uri->segment(1,0)=='dashboard' ){echo 'active';}else{echo '';} ?>">
          <a href="<?php echo site_url('dashboard/index'); ?>"><i class="fa fa-pie-chart"></i><span>Dashboard</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='kriteria' ){echo 'active';}else{echo '';} ?>">
          <a href="<?php echo site_url('kriteria/index'); ?>"><i class="fa fa-list"></i><span>Kriteria</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='alternatif'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('alternatif/index') ?>"><i class="fa fa-list-alt"></i><span>Alternatif</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='penilaian'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('penilaian/index') ?>"><i class="fa fa-calculator"></i><span>Penilaian</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='user'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('user/index') ?>"><i class="fa fa-group"></i><span>User</span></a>
        </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
