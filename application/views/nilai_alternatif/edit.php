<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Nilai Alternatif Edit</h3>
            </div>
			<?php echo form_open('nilai_alternatif/edit/'.$nilai_alternatif['']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_subkriteria" class="control-label">Id Subkriteria</label>
						<div class="form-group">
							<input type="text" name="id_subkriteria" value="<?php echo ($this->input->post('id_subkriteria') ? $this->input->post('id_subkriteria') : $nilai_alternatif['id_subkriteria']); ?>" class="form-control" id="id_subkriteria" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_alternatif" class="control-label">Id Alternatif</label>
						<div class="form-group">
							<input type="text" name="id_alternatif" value="<?php echo ($this->input->post('id_alternatif') ? $this->input->post('id_alternatif') : $nilai_alternatif['id_alternatif']); ?>" class="form-control" id="id_alternatif" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>