<?php

include_once APPPATH . '/core/Admin_controller.php';
class Kriteria extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kriteria_model');
        $this->load->model('Subkriteria_model');
        $this->load->library('');
    } 

    /*
     * Listing of kriteria
     */
    function index()
    {
        $data['_view'] = 'kriteria/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'kriteria/index';
        $this->load->view('layouts/admin_template',$data);
    }

    /*
     * Adding a new kriteria
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('kode','Kode','required');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('atribut','Atribut','required');
		$this->form_validation->set_rules('bobot','Bobot','required|integer');
		
		if($this->form_validation->run())     
        {   
            $params = array(
                'id' => str_replace("-","",$this->uuid->v4()),
				'atribut' => $this->input->post('atribut'),
				'kode' => $this->input->post('kode'),
				'nama' => $this->input->post('nama'),
				'bobot' => $this->input->post('bobot'),
                'created_at'=> date('Y-m-d H:i:s'),
                'modified_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME),
                'modified_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME)
            );
            
            $id=$this->Kriteria_model->add_kriteria($params);
            redirect('kriteria/index');
        }
        else
        {            
            $data['_view'] = 'kriteria/add';
            $data['_sidebar']='layouts/admin_sidebar';
            $data['_header']='layouts/admin_header';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a kriteria
     */
    function edit($id)
    {   
        // check if the kriteria exists before trying to edit it
        $data['kriteria'] = $this->Kriteria_model->get_kriteria($id);
        
        if(isset($data['kriteria']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('kode','Kode','required');
			$this->form_validation->set_rules('nama','Nama','required');
			$this->form_validation->set_rules('atribut','Atribut','required');
			$this->form_validation->set_rules('bobot','Bobot','required|integer');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'atribut' => $this->input->post('atribut'),
					'kode' => $this->input->post('kode'),
					'nama' => $this->input->post('nama'),
					'bobot' => $this->input->post('bobot'),
                    'modified_at' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata(SESSION_LOGIN_USERNAME)
                );

                $this->Kriteria_model->update_kriteria($id,$params);            
                redirect('kriteria/index');
            }
            else
            {
                $data['_view'] = 'kriteria/edit';
                $data['_header']='layouts/admin_header';
                $data['_sidebar']='layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The kriteria you are trying to edit does not exist.');
    } 

    /*
     * Deleting kriteria
     */
    function remove($id)
    {
        $kriteria = $this->Kriteria_model->get_kriteria($id);

        // check if the kriteria exists before trying to delete it
        if(isset($kriteria['id']))
        {
            $this->Kriteria_model->delete_kriteria($id);
            redirect('kriteria/index');
        }
        else
            show_error('The kriteria you are trying to delete does not exist.');
    }
    
    function subkriteria($id_kriteria){
        redirect('subkriteria/index/'.$id_kriteria);
    }
    function get_data_kriteria_json()
    {

        $list = $this->Kriteria_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            
            $row[] = $field->kode;
            $row[] = $field->nama;
            if($field->atribut=='1'){
                $row[] = "Benefit";
            }else if($field->atribut=='2'){
                $row[] = "Cost";
            }
            $row[] = $field->bobot;
            
            $subkriteria = $this->Subkriteria_model->get_all_subkriteria(array('subkriteria.id_kriteria'=>$field->id));
            $htmlsubkriteria="<ul>";
            foreach($subkriteria as $sbk){
                $htmlsubkriteria=$htmlsubkriteria. "<li>" .$sbk['nama'] . "</li>";
            }
            $htmlsubkriteria= $htmlsubkriteria . "<ul>";
            $row[]=$htmlsubkriteria;
            $row[] = "<a href='" .  'subkriteria/'. $field->id ."'" . "class='btn btn-primary btn-xs'><span class='fa fa-list'></span> Subkriteria</a>".
            "<br>" .
            "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Kriteria_model->count_all(),
            "recordsFiltered" => $this->Kriteria_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
