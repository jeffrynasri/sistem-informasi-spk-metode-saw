<?php
include_once APPPATH . "/core/Admin_controller.php";
class Subkriteria extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Subkriteria_model');
        $this->load->model('Kriteria_model');
    } 

    /*
     * Listing of subkriteria
     */
    function index($id_kriteria)
    {
        $data['kriteria']=$this->Kriteria_model->get_kriteria($id_kriteria);
        $data['_view'] = 'subkriteria/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }

    /*
     * Adding a new subkriteria
     */
    function add($id_kriteria)
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('isi','Isi','required');
		$this->form_validation->set_rules('nilai','Nilai','required|integer');
		
		if($this->form_validation->run())     
        {   
            $params = array(
                'id'=> str_replace("-",'',$this->uuid->v4()),
				'nama' => $this->input->post('nama'),
				'isi' => $this->input->post('isi'),
				'nilai' => $this->input->post('nilai'),
				'id_kriteria' => $id_kriteria,
                'created_at'=> date('Y-m-d H:i:s'),
                'modified_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME),
                'modified_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME)
            );
            
            $subkriteria_id = $this->Subkriteria_model->add_subkriteria($params);
            redirect('subkriteria/index/'.$id_kriteria);
        }
        else
        {            
            $data['kriteria']=$this->Kriteria_model->get_kriteria($id_kriteria);
            $data['_view'] = 'subkriteria/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a subkriteria
     */
    function edit($id_kriteria,$id)
    {   
        // check if the subkriteria exists before trying to edit it
        $data['subkriteria'] = $this->Subkriteria_model->get_subkriteria($id);
        
        if(isset($data['subkriteria']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nama','Nama','required');
			$this->form_validation->set_rules('isi','Isi','required');
			$this->form_validation->set_rules('nilai','Nilai','required|integer');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'nama' => $this->input->post('nama'),
					'isi' => $this->input->post('isi'),
					'nilai' => $this->input->post('nilai'),
					'id_kriteria' => $id_kriteria,
                    'modified_at'=>date('Y-m-d H:i:s'),
                    'modified_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME)
                );

                $this->Subkriteria_model->update_subkriteria($id,$params);            
                redirect('subkriteria/index/'.$id_kriteria);
            }
            else
            {
                $data['kriteria']=$this->Kriteria_model->get_kriteria($id_kriteria);
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $data['_view'] = 'subkriteria/edit';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The subkriteria you are trying to edit does not exist.');
    } 

    /*
     * Deleting subkriteria
     */
    function remove($id_kriteria,$id)
    {
        $subkriteria = $this->Subkriteria_model->get_subkriteria($id);

        // check if the subkriteria exists before trying to delete it
        if(isset($subkriteria['id']))
        {
            $this->Subkriteria_model->delete_subkriteria($id);
            redirect('subkriteria/index/'.$id_kriteria);
        }
        else
            show_error('The subkriteria you are trying to delete does not exist.');
    }
    function get_data_subkriteria_json($id_kriteria)
    {
        //echo $id_kriteria;
        $list = $this->Subkriteria_model->get_datatables(array('id_kriteria'=>$id_kriteria));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            
            $row[] = $field->nama;
            $row[] = $field->isi;
            $row[] = $field->nilai;
            
            $link_edit= site_url('subkriteria/edit/'. $id_kriteria . '/'.$field->id);
            $link_remove= site_url('subkriteria/remove/'. $id_kriteria."/". $field->id);
            $row[] = "<a href='" .  $link_edit ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  $link_remove ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Subkriteria_model->count_all(array('id_kriteria'=>$id_kriteria)),
            "recordsFiltered" => $this->Subkriteria_model->count_filtered(array('id_kriteria'=>$id_kriteria)),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
