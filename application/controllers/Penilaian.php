<?php

class Penilaian extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Alternatif_model');
        $this->load->model('Kriteria_model');
        $this->load->model('Subkriteria_model');
        $this->load->model('Nilai_alternatif_model');
    } 

    /*
     * Listing of nilai_alternatif
     */
    function index()
    {
        //Untuk DIOLAH
        $array_olah=array();
        $column_list_id_kriteria=array();
        // Untuk DI PRINT
        $nakol_kriteria=array();
        $nabar_alternatif=array();
        $baris_final=array();
        $baris_final_dua=array();
        
        $alternatif_list=$this->Alternatif_model->get_all_alternatif();
        $kriteria_list=$this->Kriteria_model->get_all_kriteria();

        foreach($alternatif_list as $alternatif){
            //Untuk DIOLAH
            $column_list_id_kriteria=array();
            $temp_olah=array();
            //UNTUK DIPRINT
            $nakol_kriteria=array();
            $temp=array();
            $temp_dua=array();
            array_push($nakol_kriteria,"-");
            array_push($temp,$alternatif['nama']);
            array_push($temp_dua,$alternatif['nama']);
            foreach($kriteria_list as $kriteria){
                //Untuk DIPRINT
                array_push($nakol_kriteria,$kriteria['nama']);
                $nilai_alternatif=$this->Nilai_alternatif_model->get_nilai_alternatif(
                    array('id_alternatif'=>$alternatif['id'],'id_kriteria'=>$kriteria['id'])
                );
                array_push($temp,$nilai_alternatif['subkriteria_nilai']);
                array_push($temp_dua,$nilai_alternatif['subkriteria_nama']);
                //UNTUK DIOLAH
                $temp_olah[$kriteria['id']]=$nilai_alternatif['subkriteria_nilai'];
                array_push($column_list_id_kriteria,$kriteria['id']);
            }
            //UNTUK DIOLAH
            array_push($array_olah,$temp_olah);
            array_push($nabar_alternatif,$alternatif['nama']);
            //UNTUK DIPRINT
            array_push($baris_final,$temp);
            array_push($baris_final_dua,$temp_dua);
        }
        $data['nakol_kriteria']=$nakol_kriteria;
        $data['nabar_alternatif']=$nabar_alternatif;
        $data['matriks_asli'] = $baris_final_dua;
        $data['matriks_konversi'] = $baris_final;
        $data['_view'] = 'penilaian/index';
        $data['_header']='layouts/admin_header';
        $data['_sidebar']='layouts/admin_sidebar';

        $this->form_validation->set_rules("hitung","Hitung",'required');
        if($this->form_validation->run()){
            $data['hasil_normalisasi']=$this->normalisasi($array_olah,$column_list_id_kriteria);
            $data['hasil_perangkingan']=$this->perangkingan($data['hasil_normalisasi']);
            $data['hasil_kesimpulan']=$this->hasil($data['hasil_perangkingan'],$data['nabar_alternatif']);
            // echo json_encode($this->hasil($data['hasil_perangkingan'],$data['nabar_alternatif']));
        }
        $this->load->view('layouts/admin_template',$data);
    }

    private function normalisasi($array_awal,$column_list){
        //     C1	C2	C3	C4	C5
        // A1	80	20	20	20	20
        // A2	40	40	40	40	40
        // A3	60	60	60	60	100
        // A4	80	80	80	80	80
        // A5	40	20	40	60	100
        
        // $column_list=array("0485ef15b43e44ffaa7f43f1926ed2db",'56884c403b9d4b818fe611d81b226387','96cc54f408e44cd696efafd91e73fdb6','72b53c5494544a0fa5398ee7bfba4289','6e778229b7f94e3fb6221d165c1537c7');
        // $array_awal = array(
        //     array(
        //         '0485ef15b43e44ffaa7f43f1926ed2db' => 80,
        //         '56884c403b9d4b818fe611d81b226387' => 20,
        //         '96cc54f408e44cd696efafd91e73fdb6' => 20,
        //         '72b53c5494544a0fa5398ee7bfba4289' => 20,
        //         '6e778229b7f94e3fb6221d165c1537c7' => 20,
        //     ),
        //     array(
        //         '0485ef15b43e44ffaa7f43f1926ed2db' => 40,
        //         '56884c403b9d4b818fe611d81b226387' => 40,
        //         '96cc54f408e44cd696efafd91e73fdb6' => 40,
        //         '72b53c5494544a0fa5398ee7bfba4289' => 40,
        //         '6e778229b7f94e3fb6221d165c1537c7' => 40,
        //     ),
        //     array(
        //         '0485ef15b43e44ffaa7f43f1926ed2db' => 60,
        //         '56884c403b9d4b818fe611d81b226387' => 60,
        //         '96cc54f408e44cd696efafd91e73fdb6' => 60,
        //         '72b53c5494544a0fa5398ee7bfba4289' => 60,
        //         '6e778229b7f94e3fb6221d165c1537c7' => 100,
        //     ),
        //     array(
        //         '0485ef15b43e44ffaa7f43f1926ed2db' => 80,
        //         '56884c403b9d4b818fe611d81b226387' => 80,
        //         '96cc54f408e44cd696efafd91e73fdb6' => 80,
        //         '72b53c5494544a0fa5398ee7bfba4289' => 80,
        //         '6e778229b7f94e3fb6221d165c1537c7' => 80,
        //     ),
        //     array(
        //         '0485ef15b43e44ffaa7f43f1926ed2db' => 40,
        //         '56884c403b9d4b818fe611d81b226387' => 20,
        //         '96cc54f408e44cd696efafd91e73fdb6' => 40,
        //         '72b53c5494544a0fa5398ee7bfba4289' => 60,
        //         '6e778229b7f94e3fb6221d165c1537c7' => 100,
        //     ),
        //   );
        
        $array_final=array();

        foreach($array_awal as $arr_awal){
            $temp=array();
            foreach($column_list as $col_list){
                $nilai_normalisasi=0;
                $nilai=$arr_awal[$col_list];
                $kriteria= $this->Kriteria_model->get_kriteria($col_list);
                if($kriteria['atribut']==1){
                    //benefit
                    $nilai_normalisasi = $this->normalisasi_benefit($nilai,array_column($array_awal,$col_list));
                }
                else if($kriteria['atribut']==2){
                    //cost
                    $nilai_normalisasi = $this->normalisasi_cost($nilai,array_column($array_awal,$col_list));
                }
                $temp[$col_list]=$nilai_normalisasi;
            }
            array_push($array_final,$temp);
        }

        return $array_final;
        //echo $this->normalisasi_benefit(40,array(20,40,60,80,20));
      
    }
    private function normalisasi_cost($value,$array_kriteria_value){
        return round(min($array_kriteria_value)/$value,2);
    }
    private function normalisasi_benefit($value,$array_kriteria_value){
        return round($value/max($array_kriteria_value),2);
    }

    private function perangkingan($array_normalisasi){
        $array_final=array();
        //echo json_encode($array_normalisasi);
        foreach($array_normalisasi as $arr_normalisasi){
            $hasil=0;
            foreach($arr_normalisasi as $key=>$value){
                $kriteria=$this->Kriteria_model->get_kriteria($key);
                $hasil=$hasil + ($kriteria['bobot']*$value);
            }
            array_push($array_final,$hasil);
        }
        return $array_final;
    }
    private function hasil($perangkingan,$nabar_alternatif){
        //FUSION
        $arr_fusion=array();
        $i=0;
        foreach($perangkingan as $prk){
            $temp=array();
            $temp['nama']=$nabar_alternatif[$i];
            $temp['hasil']=$prk;
            array_push($arr_fusion,$temp);
            $i++;
        }
        
        //URUTAN
        for($i=0;$i<count($arr_fusion);$i++){
            for($j=$i;$j<count($arr_fusion);$j++){
                if($arr_fusion[$i]['hasil']<$arr_fusion[$j]['hasil']){
                    $temp=$arr_fusion[$i];
                    $arr_fusion[$i]=$arr_fusion[$j];
                    $arr_fusion[$j]=$temp;
                }                
            }
        }

        return $arr_fusion;
    }
}
