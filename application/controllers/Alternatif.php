<?php
include_once APPPATH . '/core/Admin_controller.php';
 
class Alternatif extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Nilai_alternatif_model');
        $this->load->model('Alternatif_model');
        $this->load->model('Kriteria_model');
        $this->load->model('Subkriteria_model');
    } 

    /*
     * Listing of alternatif
     */
    function index()
    {
        $data['_view'] = 'alternatif/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }

    /*
     * Adding a new alternatif
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nama','Nama','required');
		
		if($this->form_validation->run())     
        {   
            $params = array(
                'id' => str_replace("-",'',$this->uuid->v4()),
				'nama' => $this->input->post('nama'),
				'keterangan' => $this->input->post('keterangan'),
                'created_at'=> date('Y-m-d H:i:s'),
                'modified_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME),
                'modified_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME)
            );
            
            $alternatif_id = $this->Alternatif_model->add_alternatif($params);
            redirect('alternatif/index');
        }
        else
        {            
            $data['_view'] = 'alternatif/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a alternatif
     */
    function edit($id)
    {   
        // check if the alternatif exists before trying to edit it
        $data['alternatif'] = $this->Alternatif_model->get_alternatif($id);
        
        if(isset($data['alternatif']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nama','Nama','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'nama' => $this->input->post('nama'),
					'keterangan' => $this->input->post('keterangan'),
                    'modified_at'=>date('Y-m-d H:i:s'),
                    'modified_by'=>$this->session->userdata(SESSION_LOGIN_USERNAME)
                );

                $this->Alternatif_model->update_alternatif($id,$params);            
                redirect('alternatif/index');
            }
            else
            {
                $data['_view'] = 'alternatif/edit';
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The alternatif you are trying to edit does not exist.');
    } 
    /*
     * Editing a alternatif
     */
    function edit_subkriteria($id)
    {   
        // check if the alternatif exists before trying to edit it
        $data['alternatif'] = $this->Alternatif_model->get_alternatif($id);
        
        if(isset($data['alternatif']['id']))
        {

            $this->form_validation->set_rules('pancingan','pancingan','required');
			if($this->form_validation->run())     
            {  
                //KOSONGKAN PENILAIAN
                $deletereturn=$this->Nilai_alternatif_model->delete_nilai_alternatif(array('id_alternatif'=>$id));
                //ISI LAGI PENILAIAN
                foreach($this->input->post('id_subkriteria') as $id_subkriteria){
                    $params = array(
                        'id_alternatif' => $id,
                        'id_subkriteria' => $id_subkriteria,
                    );
                    $return_nilai=$this->Nilai_alternatif_model->add_nilai_alternatif($params);
                }         
                redirect('alternatif/index');
            }
            else
            {
                $kriteria_list= $this->Kriteria_model->get_all_kriteria();
                $kriteria_list_final=array();
                foreach($kriteria_list as $kr_list){
                    $temp=$kr_list;
                    $temp['subkriteria']=$this->Subkriteria_model->get_all_subkriteria(array('id_kriteria'=>$kr_list['id']));
                    array_push($kriteria_list_final,$temp);
                }
                $data['kriteria'] = $kriteria_list_final;
                $data['_view'] = 'alternatif/edit_subkriteria';
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The alternatif you are trying to edit does not exist.');
    } 
    /*
     * Deleting alternatif
     */
    function remove($id)
    {
        $alternatif = $this->Alternatif_model->get_alternatif($id);

        // check if the alternatif exists before trying to delete it
        if(isset($alternatif['id']))
        {
            $this->Alternatif_model->delete_alternatif($id);
            redirect('alternatif/index');
        }
        else
            show_error('The alternatif you are trying to delete does not exist.');
    }
    function get_data_alternatif_json()
    {
        $list_kriteria=$this->Kriteria_model->get_all_kriteria();
        $list = $this->Alternatif_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $kriteria_string="<ul>";
            foreach($list_kriteria as $kriteria){
                $print_nilai_alternatif="";
                $nilai_alternatif = $this->Nilai_alternatif_model->get_nilai_alternatif(array(
                    'id_alternatif'=>$field->id,
                    'id_kriteria'=>$kriteria['id']
                ));
                //UJICOBA
                if(isset($nilai_alternatif)){
                    $print_nilai_alternatif=$nilai_alternatif['subkriteria_nama'];
                }else{
                    $print_nilai_alternatif="<b>BELUM ISI</b>";
                }
                $kriteria_string=$kriteria_string."<li>".$kriteria['nama']."<br>".$print_nilai_alternatif."</li>";                
            }
            $kriteria_string=$kriteria_string."</ul>";
            $row = array();
            
            $row[] = $field->nama;
            $row[] = $field->keterangan;
            $row[] = $kriteria_string;
            $row[] = "<a href='" .  'edit_subkriteria/'. $field->id ."'" . "class='btn btn-primary btn-xs'><span class='fa fa-pencil'></span> Isi Nilai Alternatif</a>" .
            "<br>" .
            "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Alternatif_model->count_all(),
            "recordsFiltered" => $this->Alternatif_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
