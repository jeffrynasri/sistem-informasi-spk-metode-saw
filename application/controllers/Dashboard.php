<?php
include_once APPPATH .'/core/Admin_controller.php';

class Dashboard extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Kriteria_model");
        $this->load->model("Subkriteria_model");
        $this->load->model("Alternatif_model");
        $this->load->model("User_model");
    }

    function index()
    {
        $data['_view'] = 'dashboard';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $data['kriteria']=$this->Kriteria_model->get_all_kriteria();
        $data['subkriteria']=$this->Subkriteria_model->get_all_subkriteria();
        $data['alternatif']=$this->Alternatif_model->get_all_alternatif();
        $data['user']=$this->User_model->get_all_user();
        $this->load->view('layouts/admin_template',$data);
    }
}

