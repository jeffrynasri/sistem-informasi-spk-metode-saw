<?php

class Nilai_alternatif extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Nilai_alternatif_model');
    } 

    /*
     * Listing of nilai_alternatif
     */
    function index()
    {
        $data['nilai_alternatif'] = $this->Nilai_alternatif_model->get_all_nilai_alternatif();
        
        $data['_view'] = 'nilai_alternatif/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new nilai_alternatif
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'id_subkriteria' => $this->input->post('id_subkriteria'),
				'id_alternatif' => $this->input->post('id_alternatif'),
            );
            
            $nilai_alternatif_id = $this->Nilai_alternatif_model->add_nilai_alternatif($params);
            redirect('nilai_alternatif/index');
        }
        else
        {            
            $data['_view'] = 'nilai_alternatif/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a nilai_alternatif
     */
    function edit($)
    {   
        // check if the nilai_alternatif exists before trying to edit it
        $data['nilai_alternatif'] = $this->Nilai_alternatif_model->get_nilai_alternatif($);
        
        if(isset($data['nilai_alternatif']['']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'id_subkriteria' => $this->input->post('id_subkriteria'),
					'id_alternatif' => $this->input->post('id_alternatif'),
                );

                $this->Nilai_alternatif_model->update_nilai_alternatif($,$params);            
                redirect('nilai_alternatif/index');
            }
            else
            {
                $data['_view'] = 'nilai_alternatif/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The nilai_alternatif you are trying to edit does not exist.');
    } 

    /*
     * Deleting nilai_alternatif
     */
    function remove($)
    {
        $nilai_alternatif = $this->Nilai_alternatif_model->get_nilai_alternatif($);

        // check if the nilai_alternatif exists before trying to delete it
        if(isset($nilai_alternatif['']))
        {
            $this->Nilai_alternatif_model->delete_nilai_alternatif($);
            redirect('nilai_alternatif/index');
        }
        else
            show_error('The nilai_alternatif you are trying to delete does not exist.');
    }
    
}
